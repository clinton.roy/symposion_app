# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-31 12:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('symposion_speakers', '0003_auto_20170702_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speaker',
            name='travel_assistance',
            field=models.BooleanField(default=False, help_text='Check this box if you require assistance to travel to Sydney to present your proposed sessions.', verbose_name='Travel assistance required'),
        ),
    ]
