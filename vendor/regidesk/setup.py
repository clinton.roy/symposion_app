#!/usr/bin/env python
import os
from setuptools import setup, find_packages

import regidesk


def read_file(filename):
    """Read a file into a string."""
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ''

setup(
    name="registrasion-desk",
    author="James Polley",
    author_email="jamezpolley@gmail",
    version=regidesk.__version__,
    description="Registration desk functionality for registrasion",
    url="http://gitlab.com/lca2018/registrasion-desk/",
    packages=find_packages(),
    include_package_data=True,
    classifiers=(
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python",
        "Framework :: Django",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: Apache Software License",
    ),
    install_requires=read_file("requirements.txt").splitlines(),
)
