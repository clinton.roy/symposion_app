from django import template
from django.forms import Form
import re


register = template.Library()


@register.filter
def has_required_fields(form):
    for field in form:
        if isinstance(field, Form):
            if has_required_fields(field):
                return True
        if field.field.required:
            return True
    return False


@register.filter
def has_price_fields(form):
    for field in form:
        if isinstance(field, Form):
            return has_price_fields(field)

        help_text = field.field.help_text or ''
        if '$' in help_text:
            return True

        label = field.field.label or ''
        if '$' in label:
            return True

        choices = getattr(field.field, 'choices', [])
        if choices:
            for choice_id, choice_text in choices:
                if '$' in choice_text:
                    return True

    return False


@register.filter
def any_is_void(invoices):
    for invoice in invoices:
        if invoice.is_void:
            return True
    return False


@register.filter
def contains_items_not_in(list1, list2):
    return len(set(list1).difference(list2)) > 0


@register.filter
def listlookup(lookup, target):
    try:
        return lookup[target]
    except IndexError:
        return None


@register.filter
def clean_text(txt):
    # Remove double/triple/+ spaces from `txt` and replace with single space
    return re.sub(r' {2,}' , ' ', txt)


@register.filter
def twitter_handle(txt):
    # Add @ to twitter handle if not present
    return txt if txt.startswith('@') else '@{}'.format(txt)
