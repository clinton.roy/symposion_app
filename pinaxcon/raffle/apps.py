from django.apps import AppConfig


class RaffleConfig(AppConfig):
    name = "pinaxcon.raffle"
    label = "pinaxcon_raffle"
    verbose_name = "Pinaxcon Raffle"
    admin_group_name = "Raffle admins"

    def ready(self):
        import pinaxcon.raffle.signals
    
    def get_admin_group(self):
        from django.contrib.auth.models import Group

        group, created = Group.objects.get_or_create(name=self.admin_group_name)
        return group